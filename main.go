package main

import (
	"context"
	"fmt"
	"log"
	controller "messenger/Controllers"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/rs/cors"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func goDotEnvVariable(key string) string {

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}
	return os.Getenv(key)
}
func main() {
	// Load environment variables from .env file
	dotenvUser := goDotEnvVariable("DB_USER")
	dotenvPass := goDotEnvVariable("DB_PASS")
	dotenvPort := goDotEnvVariable("PORT")

	// Connect to MongoDB
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb+srv://%s:%s@zuitt-bootcamp.izbumtb.mongodb.net/?retryWrites=true&w=majority", dotenvUser, dotenvPass))
	client, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}

	// Set up the HTTP handler
	collection := client.Database("Chat").Collection("Users")
	collection2 := client.Database("Chat").Collection("Product")

	controller.CreateUser2(collection2)
	controller.CreateUser(collection)
	controller.DeleteUser(collection)
	controller.UpdateUser(collection)
	controller.ViewUser(collection)

	// Enable CORS
	corsOptions := cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
	}
	handler := cors.New(corsOptions).Handler(http.DefaultServeMux)

	// Start the HTTP server
	port := ":" + dotenvPort
	fmt.Printf("Server listening on port %s...\n", port)
	log.Fatal(http.ListenAndServe(port, handler))
}
