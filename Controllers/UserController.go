package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type User struct {
	ID      primitive.ObjectID `json:"_id" bson:"_id,omitempty"`
	Name    string             `json:"name"`
	Age     int                `json:"age"`
	Email   string             `json:"email" bson:"email" validation:"required"`
	Message string             `json:"message"`
}

// CreateUser
func CreateUser(collection *mongo.Collection) {
	http.HandleFunc("/users/addUser", func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodPost {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		decoder := json.NewDecoder(r.Body)
		var user User
		err := decoder.Decode(&user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			fmt.Print("Wrong Format\n")
			return
		}

		existingUser := User{}
		err = collection.FindOne(context.Background(), bson.M{"Email": user.Email}).Decode(&existingUser)
		if err == nil {

			http.Error(w, "User with email already exists", http.StatusBadRequest)
			return
		}

		_, err = collection.InsertOne(context.Background(),
			bson.M{
				"Name":    user.Name,
				"Age":     user.Age,
				"Email":   user.Email,
				"Message": user.Message,
			})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(user)
	})
}


// Practice
func CreateUser2(collection *mongo.Collection) {
	http.HandleFunc("/users/addUser2", func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodPost {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		decoder := json.NewDecoder(r.Body)
		var user User
		err := decoder.Decode(&user)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			fmt.Print("Wrong Format\n")
			return
		}

		existingUser := User{}
		err = collection.FindOne(context.Background(), bson.M{"Email": user.Email}).Decode(&existingUser)
		if err == nil {

			http.Error(w, "User with email already exists", http.StatusBadRequest)
			return
		}

		_, err = collection.InsertOne(context.Background(),
			bson.M{
				"Name":    user.Name,
				"Age":     user.Age,
				"Email":   user.Email,
				"Message": user.Message,
			})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(user)
	})
}

// DeleteUser
func DeleteUser(collection *mongo.Collection) {
	http.HandleFunc("/users/deleteUser", func(w http.ResponseWriter, r *http.Request) {

		if r.Method != http.MethodDelete {
			http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			return
		}

		decoder := json.NewDecoder(r.Body)
		var user struct {
			ID string `json:"_id"`
		}

		err := decoder.Decode(&user)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		objectID, err := primitive.ObjectIDFromHex(user.ID)
		fmt.Printf("ObjectID: %v\n", objectID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		filter := bson.M{"_id": objectID}
		result, err := collection.DeleteOne(context.Background(), filter)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Printf("result: %v", result)

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(map[string]string{"message": "User deleted successfully"})
	})
}

// UpdateUser
func UpdateUser(collection *mongo.Collection) {
	http.HandleFunc("/users/updateUser", func(w http.ResponseWriter, r *http.Request) {

		decoder := json.NewDecoder(r.Body)
		var user User

		err := decoder.Decode(&user)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		objectID, err := primitive.ObjectIDFromHex(user.ID.Hex())
		fmt.Printf("ObjectID: %v\n", objectID)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		filter := bson.M{"_id": objectID}
		fmt.Printf("To update: %v", filter)

		update := bson.M{"$set": bson.M{
			"Name":    user.Name,
			"Age":     user.Age,
			"Email":   user.Email,
			"Message": user.Message,
		}}
		result, err := collection.UpdateOne(context.Background(), filter, update)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if result.MatchedCount == 0 {
			http.Error(w, "User not found", http.StatusNotFound)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(user)
	})
}

// ViewUser
func ViewUser(collection *mongo.Collection) {
	http.HandleFunc("/users/viewAll", func(w http.ResponseWriter, r *http.Request) {

		cursor, err := collection.Find(context.Background(), bson.D{})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer cursor.Close(context.Background())

		var users []User
		for cursor.Next(context.Background()) {
			var user User
			if err := cursor.Decode(&user); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			users = append(users, user)
		}
		if err := cursor.Err(); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(users)
	})
}
